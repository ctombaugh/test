package test.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import test.model.Label;
import test.service.LabelService;

@Component
public class StringToLabel implements Converter<String, Label> {

    @Autowired
    public LabelService labelService;

    @Override
    public Label convert(String source) {
        if (source == null || source.isEmpty()) {
            return null;
        }
        return labelService.findOne(Integer.parseInt(source));
    }

    public LabelService getLabelService() {
        return labelService;
    }

    public void setLabelService(LabelService labelService) {
        this.labelService = labelService;
    }

}