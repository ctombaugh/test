package test.service;

import test.model.Label;
import test.model.Test;
import java.util.List;

public interface TestService {

    List<Test> findAll();

    Test save(Test test);

    Test create();

    Test findOne(Integer id);

    Test saveAndAdd(Test test, Label label);

}