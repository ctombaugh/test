package test.service;

import test.model.Label;
import java.util.List;

public interface LabelService {

    List<Label> findAll();

    Label findOne(Integer id);

    Label create(String name);

}