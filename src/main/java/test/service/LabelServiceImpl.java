package test.service;

import org.springframework.transaction.annotation.Transactional;
import test.model.Label;
import test.model.LabelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
final class LabelServiceImpl implements LabelService {

    private final LabelRepository labelRepository;

    @Autowired
    LabelServiceImpl(LabelRepository labelRepository) {
        this.labelRepository = labelRepository;
    }

    @Override
    public List<Label> findAll() {
        return labelRepository.findAll();
    }

    @Override
    public Label findOne(Integer id) {
        return labelRepository.findOne(id);
    }

    @Override
    @Transactional
    public Label create(String name) {
        return labelRepository.save(new Label(name));
    }

}
