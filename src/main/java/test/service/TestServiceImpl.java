package test.service;

import org.springframework.transaction.annotation.Transactional;
import test.model.Label;
import test.model.Test;
import test.model.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
final class TestServiceImpl implements TestService {

    private final TestRepository testRepository;

    @Autowired
    TestServiceImpl(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    @Override
    public List<Test> findAll() {
        return testRepository.findAll();
    }

    @Override
    @Transactional
    public Test save(Test test) {
        return testRepository.save(test);
    }

    @Override
    @Transactional
    public Test saveAndAdd(Test test, Label label) {
        test = testRepository.save(test);
        test.getLabels().add(label);
        return testRepository.save(test);
    }

    @Override
    @Transactional
    public Test create() {
        return testRepository.save(new Test());
    }

    @Override
    public Test findOne(Integer id) {
        return testRepository.findOne(id);
    }

}
