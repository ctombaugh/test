package test.web;

import test.model.Label;
import test.model.Test;

public class TestForm {

    private Test test;
    private Label label;

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }
    
}
