package test.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import test.model.Label;
import test.model.Test;
import test.service.LabelService;
import test.service.TestService;
import java.util.List;

@Controller
@RequestMapping("/")
public class TestController {

    private TestService testService;
    private LabelService labelService;

    private static final Logger logger = Logger.getLogger(TestController.class);

    @Autowired
    TestController(TestService testService, LabelService labelService) {
        this.testService = testService;
        this.labelService = labelService;

        // populate labels
        List<Label> labels = labelService.findAll();
        if (labels == null || labels.size() == 0) {
            labelService.create("label a");
            labelService.create("label b");
        }

    }

    @ModelAttribute("labels")
    public List<Label> getLabels() {
        return labelService.findAll();
    }

    @ModelAttribute("user")
    public String getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        return name;
    }

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String firstPage(ModelMap model) {
        List<Test> tests = testService.findAll();
        model.addAttribute("tests", tests);
        return "index";
    }

    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String login(ModelMap model) {
        return "login";
    }

    @RequestMapping(value="/addtest", method = RequestMethod.GET)
    public String addExpert(ModelMap model) {
        testService.create();
        return "redirect:/";
    }

    @RequestMapping(value="/test/{testId}", method=RequestMethod.GET)
    public String editProject(@PathVariable Integer testId, ModelMap model) {
        TestForm form = new TestForm();
        Test test = testService.findOne(testId);
        form.setTest(test);
        model.addAttribute("testForm", form);
        return "test";
    }

    @RequestMapping(value="/test", method=RequestMethod.POST)
    public String submitTest(@ModelAttribute("testForm") TestForm testForm, BindingResult result) {
        logger.info("labels: " + testForm.getTest().getLabels());
        if (testForm.getTest().getLabels() != null) {
            logger.info("labels size: " + testForm.getTest().getLabels().size());
        }
        testService.save(testForm.getTest());
        return "redirect:/";
    }

    @RequestMapping(value="/addlabel", method=RequestMethod.POST)
    public String addLabel(@ModelAttribute("testForm") TestForm testForm, BindingResult result, ModelMap model) {
        Test test = testForm.getTest();
        Label label = testForm.getLabel();
        test = testService.saveAndAdd(test, label);
        return "redirect:/test/" + test.getId();
    }
}
